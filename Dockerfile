FROM python:3.11-slim-bookworm

RUN apt update -y && apt install -y pipenv

WORKDIR /app

RUN pipenv install setuptools

COPY . /app

RUN pipenv install

CMD pipenv run python -u ./snooapiclient/__main__.py
