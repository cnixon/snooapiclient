import json
import os
from pprint import pprint

import boto3

from pubnub.pnconfiguration import PNConfiguration
from pubnub.enums import (
    PNHeartbeatNotificationOptions,
    PNStatusCategory,
    PNOperationType,
)
from pubnub.pubnub import PubNub
from pubnub.callbacks import SubscribeCallback

import requests

USERNAME = os.environ["SNOO_USERNAME"]
PASSWORD = os.environ["SNOO_PASSWORD"]
PN_PUB_ID = os.environ["PN_PUB_ID"]
PN_SUB_ID = os.environ["PN_SUB_ID"]
CLIENT_ID = os.environ["CLIENT_ID"]


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token, refresh_token=None, expires_in=None):
        self.token = token

    def __call__(self, r):
        # TODO check expires_at and refresh if necessary
        r.headers["authorization"] = "Bearer " + self.token
        return r


API_HOST_ROOT = "https://api-us-east-1-prod.happiestbaby.com"
USER_AUTH_DEVELOPER_PROVIDER_PATH = API_HOST_ROOT + f"/us/users/{USERNAME}/auth-data"

COUNTRIES_ENDPOINT = API_HOST_ROOT + "/us/countries?language=en"
SETTINGS_ENDPOINT = API_HOST_ROOT + "/us/me/v10/settings"
BABY_ENDPOINT = API_HOST_ROOT + "/us/me/v10/baby"
NOTIFICATIONS_ENDPOINT = API_HOST_ROOT + "/ns/me/v10/notifications/timezone"

DEVICES_ENDPOINT = API_HOST_ROOT + "/hds/me/v11/devices"
ME_ENDPOINT = API_HOST_ROOT + "/us/me/v10/me"

PUBNUB_AUTHORIZE_ENDPOINT = API_HOST_ROOT + "/us/me/v10/pubnub/authorize"

resp = requests.get(USER_AUTH_DEVELOPER_PROVIDER_PATH.format(USERNAME))

# print(resp.json())

cognito_ids = resp.json()["cognitoIds"]

client = boto3.client("cognito-idp", region_name=cognito_ids["region"])

response = client.initiate_auth(
    AuthFlow="USER_PASSWORD_AUTH",
    ClientId=cognito_ids["clientId"],
    AuthParameters={"USERNAME": USERNAME, "PASSWORD": PASSWORD},
)

auth_result = response["AuthenticationResult"]
access_token = auth_result["AccessToken"]
expires_in = auth_result["ExpiresIn"]
id_token = auth_result["IdToken"]
refresh_token = auth_result["RefreshToken"]
token_type = auth_result["TokenType"]

# pprint(
#     {
#         "auth_result": auth_result,
#         "access_token": access_token,
#         "expires_in": expires_in,
#         "id_token": id_token,
#         "refresh_token": refresh_token,
#         "token_type": token_type,
#     }
# )

bearer_auth = BearerAuth(id_token, refresh_token, expires_in)

pubnub_authorize = requests.post(PUBNUB_AUTHORIZE_ENDPOINT, auth=bearer_auth)

countries = requests.get(COUNTRIES_ENDPOINT, auth=bearer_auth)
settings = requests.get(SETTINGS_ENDPOINT, auth=bearer_auth)
baby = requests.get(BABY_ENDPOINT, auth=bearer_auth)

baby_id = baby.json()["_id"]

BABIES_ENDPOINT = API_HOST_ROOT + f"/ss/me/v10/babies/{baby_id}"

BABIES_LAST_SESSION_ENDPOINT = BABIES_ENDPOINT + "/sessions/last"

# Obviously a GET with start time, start of day, levels, detailed levels and timezone queryparams
# BABIES_SESSION_DAILY = BABIES_ENDPOINT + "/sessions/daily?startTime=2023-10-30%2007%3A00%3A00.000&levels=true&detailedLevels=true&timezone=Europe%2FLondon"

baby_last_session = requests.get(BABIES_LAST_SESSION_ENDPOINT, auth=bearer_auth)

# pprint(countries.json())
# pprint(settings.json())
# pprint(baby.json())
# pprint(baby_last_session.json())

pubnub_resp_body = pubnub_authorize.json()
# pprint(pubnub_resp_body)

pubnub_token = pubnub_resp_body["snoo"]["token"]
pubnub_snoo_serial = pubnub_resp_body["snoo"]["serialNumbers"][0]
pubnub_expires_in = pubnub_resp_body["expiresIn"]

pnconfig = PNConfiguration()
# subscribe_key from Admin Portal
pnconfig.subscribe_key = PN_SUB_ID
# publish_key from Admin Portal (only required if publishing)
pnconfig.publish_key = PN_PUB_ID

pnconfig.uuid = CLIENT_ID


class MySubscribeCallback(SubscribeCallback):
    def __init__(self, mqtt_client, snoo_serial):
        self.mqtt_client = mqtt_client
        self.snoo_serial = snoo_serial

    def presence(self, pubnub, presence):
        pass  # handle incoming presence data
        # TODO: Add status updates

    def status(self, pubnub, status):
        if status.category == PNStatusCategory.PNUnexpectedDisconnectCategory:
            pass  # This event happens when radio / connectivity is lost

        elif status.category == PNStatusCategory.PNConnectedCategory:
            # Connect event. You can do stuff like publish, and know you'll get it.
            # Or just use the connected event to confirm you are subscribed for
            # UI / internal notifications, etc
            print("connected")
            ## pubnub.publish().channel('my_channel').message('Hello world!').pn_async(my_publish_callback)
        elif status.category == PNStatusCategory.PNReconnectedCategory:
            pass
            # Happens as part of our regular operation. This event happens when
            # radio / connectivity is lost, then regained.
        elif status.category == PNStatusCategory.PNDecryptionErrorCategory:
            pass
            # Handle message decryption error. Probably client configured to
            # encrypt messages and on live data feed it received plain text.

    def message(self, pubnub, message):
        # Handle new message stored in message.message
        print("Received Message:")
        pprint(message.message)
        self.mqtt_client.publish(
            f"snoo/{self.snoo_serial}",
            payload=json.dumps(message.message),
            qos=1,
            retain=False,
        ).wait_for_publish()
        print("published")

pubnub_client = PubNub(pnconfig)
