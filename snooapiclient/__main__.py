#!/usr/bin/env python

import json
import os

# from pprint import pprint

import boto3

import paho.mqtt.client as mqtt

from snoo import pubnub_client, pubnub_snoo_serial, pubnub_token, MySubscribeCallback

MQTT_HOST = os.environ["MQTT_HOST"]
MQTT_PORT = int(os.environ["MQTT_PORT"] or 1883)

mqtt_client = mqtt.Client(client_id="snoo-sensor", clean_session=False)
# mqtt_client.connect("mosquitto", 1883, 60)
mqtt_client.connect(MQTT_HOST, MQTT_PORT, keepalive=10)
mqtt_client.reconnect_delay_set()
mqtt_client.loop_start()


def snoo_mqtt_sensor_payload(serial, name, value, additional_options=None):
    additional_options = additional_options or {}
    base = {
        "name": name,
        "unique_id": f"{serial}-{value.replace('.', '_')}",
        "state_topic": f"snoo/{serial}",
        "value_template": '{{ value_json.' + f"{value}" + '}}',
        "device": {
            "identifiers": [serial],
            "name": "Snoo",
            "model": "Snoo Bassinet",
            "manufacturer": "Happiest Baby",
        },
    }
    return {**base, **additional_options}


# TODO Subscribe to mqtt homeassistant/status top and publish discovery
# messages when an "online" message is detected

# TODO add types, eg binary for hold/audio/weaning

snoo_states = ["BASELINE", "LEVEL1", "LEVEL2", "LEVEL3", "LEVEL4"]

sensors = [
    (
        "State",
        "state_machine.state",
        {"device_class": "ENUM", "options": snoo_states + ["ONLINE", "PRETIMEOUT"]},
    ),
    (
        "Up Transition",
        "state_machine.up_transition",
        {"device_class": "ENUM", "options": snoo_states + ["NONE"]},
    ),
    (
        "Down Transition",
        "state_machine.down_transition",
        {"device_class": "ENUM", "options": snoo_states + ["NONE"]},
    ),
    ("Session Id", "state_machine.session_id", None),
    ("Active Session", "state_machine.is_active_session", None),
    ("Audio", "state_machine.audio", None),
    ("Hold", "state_machine.hold", None),
    ("Time Left", "state_machine.time_left", {"device_class": "DURATION","native_unit_of_measurement": "s" }),
    ("Session Duration", "state_machine.since_session_start_ms", {"device_class": "DURATION","native_unit_of_measurement": "ms" }),
    ("White Noise Stickiness", "state_machine.sticky_white_noise", None),
    ("Weaning", "state_machine.weaning", None),
    ("Last Event", "event", None),
    ("Last Event Time", "event_time_ms | timestamp_local", {"device_class": "TIMESTAMP", "native_value": "datetime.datetime"}),
    ("Left Safety Clip", "left_safety_clip", None),
    ("Right Safety Clip", "right_safety_clip", None),
    ("System State", "system_state", {"device_class": "ENUM"}),
]

for name, value, additional_options in sensors:
    mqtt_client.publish(
        f"homeassistant/sensor/test-snoo-{pubnub_snoo_serial}-{value.replace('.', '_')}/config",
        payload=json.dumps(
            snoo_mqtt_sensor_payload(
                pubnub_snoo_serial,
                name,
                value,
                additional_options=additional_options
            )
        ),
        qos=1,
        retain=False,
    ).wait_for_publish()

pubnub_client.add_listener(MySubscribeCallback(mqtt_client, pubnub_snoo_serial))

pubnub_client.set_token(pubnub_token)

pubnub_client.publish().channel("ControlCommand." + pubnub_snoo_serial).message({"command":"send_status"}).sync()

pubnub_client.subscribe().channels("ActivityState." + pubnub_snoo_serial).execute()
